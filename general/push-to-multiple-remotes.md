# Pushing to multiple remotes

GitLab's code is available at [gitlab.com] and [dev.gitlab.org]. This guide
provides simple methods for pushing changes to both locations at once.

## Using `hub`

[hub] is a wrapper around the `git` command that adds a few niceties, one of
which is the ability to push to multiple remotes in a single command by
separating them with a comma.

See the [installation instructions](https://github.com/github/hub#installation)
to get started.

For the example below, it's assumed that `hub` is installed and that you have
remotes called `origin` and `dev`, pointing to the appropriate repository at
[gitlab.com] and [dev.gitlab.org], respectively.

You can push the `master` branch to all both at once like this:

```sh
hub push origin,dev master
```

## Using a Bash script (advanced)

Add this script to your `~/.bashrc`, `~/.zshrc`, etc.:

```sh
gpa ()
{
  git push origin ${1:-master} && git push dev ${1:-master}
}
```

Again, this assumes that you have remotes called `origin` and `dev`, pointing to
the appropriate repository at [gitlab.com] and [dev.gitlab.org], respectively.

Then you can push to both at once like this:

```sh
# Defaults to `master`
gpa

# Push to `feature-1`
gpa feature-1
```

## Add remotes to each repo

### `gitlab-ce`

  1. Add a remote for `dev` in `gitlab-ce`:
      ```sh
      git remote add dev git@dev.gitlab.org:gitlab/gitlabhq.git
      ```
  1. Verify it was added by running:
      ```sh
      git remote -v
      ```
      You should see:
      ```
      dev	git@dev.gitlab.org:gitlab/gitlabhq.git (fetch)
      dev	git@dev.gitlab.org:gitlab/gitlabhq.git (push)
      origin	https://gitlab.com/gitlab-org/gitlab-ce.git (fetch)
      origin	https://gitlab.com/gitlab-org/gitlab-ce.git (push)
      ```

### `gitab-ee`

  1. Add a remote for `dev` in `gitlab-ee`
      ```sh
      git remote add dev git@dev.gitlab.org:gitlab/gitlab-ee.git
      ```
  1. Verify it was added by running:
      ```sh
      git remote -v
      ```
      You should see:
      ```
      dev    git@dev.gitlab.org:gitlab/gitlab-ee.git (fetch)
      dev    git@dev.gitlab.org:gitlab/gitlab-ee.git (push)
      origin https://gitlab.com/gitlab-org/gitlab-ee.git (fetch)
      origin https://gitlab.com/gitlab-org/gitlab-ee.git (push)
      ```

### `omnibus-gitlab`

  1. Add a remote for `dev` in `omnibus-gitlab`:
      ```sh
      git remote add dev git@dev.gitlab.org:gitlab/omnibus-gitlab.git
      ```
  1. Verify it was added by running:
      ```sh
      git remote -v
      ```
      You should see:
      ```
      dev    git@dev.gitlab.org:gitlab/omnibus-gitlab.git (fetch)
      dev    git@dev.gitlab.org:gitlab/omnibus-gitlab.git (push)
      origin git@gitlab.com:gitlab-org/omnibus-gitlab.git (fetch)
      origin git@gitlab.com:gitlab-org/omnibus-gitlab.git (push)
      ```

## Local stable branch
**Make sure your local stable branch is up to date.**

1. Check out each branch in each repo
    ```sh
    git checkout -b X-Y-stable -t origin/X-Y-stable
    ```
1. Run `git log` and verify, for each repo, that your local stable branch is up to date

## Sync each remote
1. `gitlab-ce`
  Push the stable branch to `origin` and `dev`:
    ```sh
      hub push origin,dev X-Y-stable
    ```

    Make sure each remote was correctly updated:
      1. origin: `https://gitlab.com/gitlab-org/gitlab-ce/commits/X-Y-stable`
      1. dev: `https://dev.gitlab.org/gitlab/gitlabhq/commits/X-Y-stable`

1. `gitlab-ee`
  Push the stable branch to `origin` and `dev`:
    ```sh
      hub push origin,dev X-Y-stable-ee
    ```

    Make sure the remote was correctly updated:
      1. origin: `https://gitlab.com/gitlab-org/gitlab-ee/commits/X-Y-stable-ee`
      1. dev: `https://dev.gitlab.org/gitlab/gitlab-ee/commits/X-Y-stable-ee`

1. `omnibus-gitlab`
  Push the CE and EE stable branches to `origin` and `dev`:
    ```sh
    hub push origin,dev X-Y-stable
    hub push origin,dev X-Y-stable-ee
    ```
    Make sure each remote was correctly updated:
      1. origin: `https://gitlab.com/gitlab-org/omnibus-gitlab/commits/X-Y-stable`
      1. origin: `https://gitlab.com/gitlab-org/omnibus-gitlab/commits/X-Y-stable-ee`
      1. dev: `https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/X-Y-stable`
      1. dev: `https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/X-Y-stable-ee`

[gitlab.com]:     https://gitlab.com/gitlab-org/
[dev.gitlab.org]: https://dev.gitlab.org/gitlab/
[hub]:            https://github.com/github/hub

---

[Return to Guides](../README.md#guides)
