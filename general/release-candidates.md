# Creating Release Candidates

Release Candidates (RCs) are pre-release versions of the next version of
GitLab CE and EE. The first RC, appropriately called RC1, is typically created
as close as possible during the release of each month.

There should be no code changes between the final RC and what is released to the
public.

## About the "Release Candidate" naming

We call them "Release Candidates" as this simplifies our releasing/packaging
tools and scripts. This approach is coherent with packages.gitlab.com since our
RC packages are available under [`gitlab/unstable`].

## Guides

- [Creating an RC prior to feature freeze](#prior-to-feature-freeze)
- [Feature freeze RC](#feature-freeze-rc)
- [Creating subsequent RCs](#creating-subsequent-rcs)

### Prior to Feature Freeze

A release issue should already be created.  Follow the monthly guide if it does
not already exist: [Monthly Release](monthly.md)

#### Step 1: Pick a commit from X-Y-auto-deploy-XXXXXXXX

Select a commit from our auto-deploy branch which will be used as the "cut" for
this release (everything up to this commit in `auto-deploy` branch will be
included in the release). It is advised to make the cut from a recent green
build as to avoid problems during pipeline test failures.

#### Step 2: Integrating changes from `auto-deploy` into `X-Y-stable`

##### RC1

If this is the first RC we need to create our stable branch

```
git checkout <chosen sha of step 1>
git checkout -b X-Y-stable
git push -u origin X-Y-stable
```

Once the `X-Y-stable` branch is created, it is the sole source of future
releases for that version. From the freeze date, merge requests will either
be [cherry-picked] into `X-Y-stable` by the release manager.

Developers are responsible for notifying the release manager that a merge
request is ready to be moved into `X-Y-stable` if required.

##### RCX

If this is not the first RC we simply need to update our stable branch

```
# checkout to the stable branch for the current RC in gitlab-ce
git checkout X-Y-stable
git merge <sha> --no-ff
git push

# and for EE branch in gitlab-ee
git checkout X-Y-stable-ee
git merge <ce/X-Y-stable> --no-ff
git push
```

#### Step 3: Sync omnibus `X-Y-stable` & `X-Y-stable-ee` with master

```sh
git checkout master
git pull origin
git checkout -b X-Y-stable
git push -u origin
git checkout master
git checkout -b X-Y-stable-ee
git push -u origin
```

---

### Creating subsequent RCs

Create a release issue by running the following in Slack:

```sh
/chatops run release_issue MAJOR.MINOR.0-rcX
```

Replace `MAJOR.MINOR` with the major and minor version, and `X` with the RC
version. For example:

```sh
/chatops run release_issue 11.8.0-rc2
```

Once created, follow the steps outlined in the release issue.

[`gitlab/unstable`]: https://packages.gitlab.com/gitlab/unstable
[cherry-picked]: pick-changes-into-stable.md

---

[Return to Guides](../README.md#guides)
